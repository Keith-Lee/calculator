//
//  AppDelegate.h
//  CalculatorWithDot
//
//  Created by Lavita on 2014/7/6.
//  Copyright (c) 2014年 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
