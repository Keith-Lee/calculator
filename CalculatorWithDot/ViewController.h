//
//  ViewController.h
//  CalculatorWithDot
//
//  Created by Lavita on 2014/7/6.
//  Copyright (c) 2014年 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Calculator.h"

@interface ViewController : UIViewController <ViewDelegate>

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *numberButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *signButtons;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (strong, nonatomic) Calculator *myCalculator;
@property (nonatomic) BOOL isTypingNumber;
@property (strong, nonatomic) NSString *testString;
- (IBAction)clearPressed:(id)sender;

- (IBAction)numberPressed:(id)sender;
- (IBAction)signPressed:(id)sender;
- (IBAction)dotPressed:(id)sender;

@end
