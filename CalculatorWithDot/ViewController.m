//
//  ViewController.m
//  CalculatorWithDot
//
//  Created by Lavita on 2014/7/6.
//  Copyright (c) 2014年 test. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)didReset{
    NSLog(@"delegate: reset");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"view did load");
    self.myCalculator = [[Calculator alloc] init];
    self.myCalculator.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotificationOfReset) name:@"didReset" object:nil];
}

- (void)receiveNotificationOfReset{
    NSLog(@"reset");
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    NSLog(@"will layout");
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"view will appear");
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    NSLog(@"view did layout");
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"View did appeared");

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clearPressed:(id)sender {
    self.resultLabel.text= @"0";
    [self.myCalculator reset];
    
}

- (IBAction)numberPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    if([self.resultLabel.text intValue] == 0 || !self.isTypingNumber){
        self.resultLabel.text = button.titleLabel.text;
        self.isTypingNumber = YES;
    }else{
        NSString *originalNumberString = self.resultLabel.text;
        NSString *newString = [originalNumberString stringByAppendingString:button.titleLabel.text];
        self.resultLabel.text = newString;
    }
}

- (IBAction)signPressed:(id)sender {
    double op1 = [self.resultLabel.text doubleValue];
    self.myCalculator.oper1= op1;
    if (!self.myCalculator.hasOper2Set) {
        self.resultLabel.text = @"";
        [self.myCalculator operate];
        UIButton *button = (UIButton *)sender;
        NSString *sign = button.titleLabel.text;
        self.myCalculator.operatingSign = sign;
    }else{
        double result = [self.myCalculator operate];
        UIButton *button = (UIButton *)sender;
        NSString *sign = button.titleLabel.text;
        self.myCalculator.operatingSign = sign;
        self.resultLabel.text = [NSString stringWithFormat:@"%f", result];
    }
    
    self.isTypingNumber = NO;
    
}

- (IBAction)dotPressed:(id)sender {
    NSRange rangeOfDot = [self.resultLabel.text rangeOfString:@"."];
    if (rangeOfDot.length == 0) {
        NSString *newString = [self.resultLabel.text stringByAppendingString:@"."];
        self.resultLabel.text =newString;
    }
}
@end
