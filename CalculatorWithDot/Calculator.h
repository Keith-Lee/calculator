//
//  Calculator.h
//  CalculatorWithDot
//
//  Created by Lavita on 2014/7/6.
//  Copyright (c) 2014年 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ViewDelegate <NSObject>

- (void)didReset;

@end

@interface Calculator : NSObject
@property (strong, nonatomic) NSString *operatingSign;
@property double oper1,oper2;
@property BOOL hasOper2Set;

@property (strong, nonatomic) id<ViewDelegate> delegate;

- (double)operate;
- (void)reset;

@end
