//
//  Calculator.m
//  CalculatorWithDot
//
//  Created by Lavita on 2014/7/6.
//  Copyright (c) 2014年 test. All rights reserved.
//

#import "Calculator.h"

@interface Calculator ()

@end

@implementation Calculator
- (double)operate{
    if(self.hasOper2Set){
        NSLog(@"Calculateing oper1 : %f, oper2 : %f\nwith sign : %@",self.oper1,self.oper2, self.operatingSign);
        
        if([self.operatingSign isEqualToString:@"+"]){
            self.oper2 = self.oper2 + self.oper1;
        }
        else if([self.operatingSign isEqualToString:@"-"]){
            self.oper2= self.oper2- self.oper1;
        }
        else if([self.operatingSign isEqualToString:@"X"]){
            self.oper2= self.oper2* self.oper1;
        }
        else if([self.operatingSign isEqualToString:@"/"]){
            if(self.oper1 != 0){
                self.oper2= self.oper2/ self.oper1;
            }
        }else if ([self.operatingSign isEqualToString:@"="]){
            self.oper2 = self.oper1;
        }
        
        return self.oper2;
    }else{
        self.oper2=self.oper1;
        self.hasOper2Set=YES;
        return self.oper2;
    }
    
}

- (void)reset{
    self.hasOper2Set = NO;
    self.operatingSign = @"";
    
    [self.delegate didReset];
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"didReset" object:nil];
}

@end
